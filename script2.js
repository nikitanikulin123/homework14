document.querySelector('.sign_in').addEventListener('click', function(e) {
    e.preventDefault();
    var email = document.querySelector('.email').value;
    var password = document.querySelector('.password').value;
    var xhr = new XMLHttpRequest();
    var body = 'email=' + encodeURIComponent(email) + '&password=' + encodeURIComponent(password);

    xhr.open('POST', 'http://netology-hj-ajax.herokuapp.com/homework/login_xml', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.addEventListener('loadstart', function() {
        document.querySelector('.preloader').style.display = 'block';
    });

    xhr.addEventListener('loadend', function() {
        document.querySelector('.preloader').style.display = 'none';
    });

    xhr.addEventListener('load', function() {
        var errors = document.querySelector('.errors');
        var avatar = document.querySelector('.avatar');
        var fullname = document.querySelector('.fullname');
        var country = document.querySelector('.country');
        var hobbies = document.querySelector('.hobbies');
        document.querySelector('.sign_in_form').style.display = 'none';
        document.querySelector('.sign_out_form').style.display = 'block';
        if(xhr.status === 200) {
            var myXML = parseXml(xhr.responseText);

            avatar.attributes[0].value = getXmlElement('userpic');
            fullname.innerHTML = 'fullname: ' + getXmlElement('name') + getXmlElement('lastname');
            country.innerHTML = 'country: ' + getXmlElement('country');
            hobbies.innerHTML = 'hobbies: ' + getXmlElement('hobbies');

            function parseXml(str) {
                return (new DOMParser()).parseFromString(str, "application/xml");
            }

            function getXmlElement(xmlElement) {
                return myXML.getElementsByTagName(xmlElement)[0].textContent;
            }
        } else {
            errors.innerHTML = 'ERROR! ' + xhr.statusText;
        }
    });

    xhr.send(body);
});

document.querySelector('.sign_out').addEventListener('click', function(e) {
    document.querySelector('.sign_in_form').style.display = 'block';
    document.querySelector('.sign_out_form').style.display = 'none';
});